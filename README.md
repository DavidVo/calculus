# Calculus

-- This is a training project -- 

Calculus
=======

### Formation JEE

Ceci est mon premier projet avec **GIT**
 
### Utilisation de git début
* Creation d' un projet GIT
* Récupertion sur l'ordinateur dans _mes douments (cd Documents)_ via la commande dans le terminal _git clone https://gitlab.com/DavidVo/calculus.git_
* Modification du fichier _README.md (celui ci)_ en utilisant le language markdown
* Configuration du GIT avec _git config --global user.name DavidVo_ et _git config --global user.email david.vogel26@gmail.com_
* Pour voir une config en particulier : git config --global --get user.name
* Pour lister les Config : git config --global --list
* Pour indiquer a GIT de modifier les fichiers au prochain commit : _git add README.md_
* Pour mettre dans le commit tout ce qui a été modif : git add --all
* Pour synchroniser avec GitLab : git push
* Pour renomer un depot a distance, il faut commencer par recuperer l'adresse du depot a distance avec : git remote -v, puis git remote add NOM_VOULU ADRESSE_DEPOT
* Pour stocker dans un depot specifique git push NOM_DU_DEPOT (dans ce cas la rep local)
* Pour renommer un server git rename NOM_ORIGINAL NEW_NAME
* Pour que certains fichier ne soit pas envoyé vers Git quand on fait --all d'un repertoire on peut créer un .gitignore dans ce même repertoire
* dans le .gitignore le  *.class  permet de ne pas envoyé tout les fichier .class le  \**/choux  permet de ne pas envoyer tout les fichier contenant choux
* Pour supprimer un fichier uploader par erreur sur git : git rm --cached NOM_FICHER puis commit and push
* Pour cloner dans un noueau repertoire : git clone https://gitlab.com/DavidVo/calculus.git NOM_DU_REP_A_CREER
* Pour updater un fichier on fait : git pull

### Notion de branche
* Pour aller chercher les changement des fichier ayant été modifié sur GIT (internet) et les repatrier en local : git fetch
* Pour prendre toutes les versions de plusieurs dêpots on peut faire : git fetch --all
* Pour preciser un dêpot  en particulier on peut faire git fetch NOM_DU_DEPOT
* A chaque fois que l'on fait un commit on créer une branche 
* Pour fusionner ce qu'on vient juste de "fetcher" on fait : git merge FETCH_HEAD
* (Ce 2 dernière étapes reviennent au même que faire git pull ou git pull origin)
* Pour faire une list des des branches : git branch. Pour avoir un peu plus d'information on peut faire git branch -v, donne en plus le nom du dernier commit

